var Discord = require('discord.io');

const discord_data_limit = 2000;

var bot = new Discord.Client({
    autorun: true,
    token: mbConfig.discord_token
});

bot.on('ready', function(event) {
    console.log('Logged in as %s - %s\n', bot.username, bot.id);
});

bot.on('disconnect', function(errMsg, code) {
    bot.connect();

    // Delay for an interval before reconnect.
    setTimeout((function () {
        logger.info("Attempting to reconnect to Discord...");
        bot.connect();
    }), mbConfig.reconnection_delay); // end setTimeout()

});

bot.on('message', function(user, userID, channelID, message, event) {
    //console.log("EVENT: " + JSON.stringify(event));
    //console.log("MESSAGE: " + JSON.stringify(message));
    //console.log("GUILD: " + JSON.stringify(message.guild));
    //console.log("DMs: " + JSON.stringify(bot.directMessages));
    //console.log("DM?: " + discord_isDM(channelID));

    // Respond on all channels.
    if (message[0] == mbConfig.discord_trigger) {
    // Check trigger and channelIDs listening.
    //if (message[0] == mbConfig.discord_trigger && (mbConfig.discord_channelIDs.indexOf(channelID) != -1 || discord_isDM(channelID)) ) {
        discord_parse_message(user, userID, channelID, message.substring(1), event);
    }
});

function discord_parse_message(user, userID, channelID, message, event) {
    console.debug("discord_parse_message() called by " + user + ": " + message);

    trigger = message.split(' ')[0];
    args = message.split(' ').splice(1);
    console.debug("- args: " + JSON.stringify(args));

    switch(trigger.toLowerCase()) {
        case 'contract':
        case 'c':
        case 'fd':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.contract)
            break;

        case 'dungeon':
        case 'dg':
        case 'd':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.dungeon)
            break;

        case 'effect':
        case 'ef':
        case 'e':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.effect)
            break;

        case 'enemymod':
        case 'enm':
        case 'em':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.mod_enemy)
            break;

        case 'job':
        case 'j':
        case 'jb':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.job)
            break;

        case 'legendary':
        case 'efl':
        case 'l':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.legendary)
            break;

        case 'matchbornes':
        case 'mb':
        case 'search':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes(channelID, args);
            break;

        case 'skillmod':
        case 'sm':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.mod_skill)
            break;

        case 'ping':
            bot.simulateTyping(channelID);
            discord_parse_pong(channelID);
            break;

        case 'skill':
        case 'sk':
        case 's':
            bot.simulateTyping(channelID);
            discord_parse_matchbornes_category(channelID, args, searchtype.skill)
            break;

        case 'version':
        case 'ver':
            bot.simulateTyping(channelID);
            discord_parse_version(channelID);
            break;

    }
}

function discord_isDM(channelID) {
    if (Object.keys(bot.directMessages).indexOf(channelID) != -1) {
        return true;
    }
    return false;
}


function discord_parse_matchbornes(channelID, args) {
    var pattern = args.join(' ');

    if (pattern[0] == '@') {
        // exact match
        var matches = discord_matchbornes(pattern.substring(1), true);
    } else {
        var matches = discord_matchbornes(pattern, false);
    }
    
    console.log("discord_parse_matchbornes() received result: " + JSON.stringify(matches));
    //var msg = "mb: " + pattern + "  matches: " + matches.length + "\n\n" + matches;
    bot.sendMessage({
        to: channelID,
        message: matches,
    });
    return true;    
}

function discord_parse_matchbornes_category(channelID, args, category) {
    var pattern = args.join(' ');

    var matches = discord_matchbornes_category(category, pattern);

    
    //console.log("matches data length: " + matches.length);
    if (matches.length > discord_data_limit) {
        matches = "_Too many results. Search with more specific criteria, or use the web._"
    }
    
    bot.sendMessage({
        to: channelID,
        message: matches,
    });
    return true; 
}

function discord_parse_pong(channelID) {
    bot.sendMessage({
        to: channelID,
        message: "pong",
    });
    return true;    
}

function discord_parse_version(channelID) {
    verstring = "matchbornes v" + version + ". " + parseInt(japanese.length + prepared_japanese.length) + " records loaded per language.";
    bot.sendMessage({
        to: channelID,
        message: verstring,
        //typing: true
    });
    return true;
}


function discord_matchbornes(pattern, exact) {
    try {
        pattern = decodeURI(pattern);
        console.log("discord_matchbornes() called: " + JSON.stringify(pattern) + "\t Exact: " + exact);

        var matches = [];
        var matches_prepared = [];
    
        for (r in japanese) {
            try {
                if (
                    // Check IDs
                    japanese[r].id.toUpperCase().includes(pattern.toUpperCase()) ||
        
                    // Check Data
                    japanese[r].msg.includes(pattern) ||
                    //match_data(japanese, r, pattern) ||
                    chinese_traditional[r].msg.includes(pattern) ||
                    chinese_traditional_current[r].msg.includes(pattern) ||
                    //|| chinese_simplified[r].msg.includes(pattern)                
                    korean[r].msg.includes(pattern) ||
                    english_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    english[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    french[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    french[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    spanish[r].msg.toUpperCase().includes(pattern.toUpperCase())
    
                ) {
                    matches.push(r);
                }
            } catch (err) {
                console.log("discord_matchbornes(): ERROR processing original.pot matches: " + err);
            }
    

        }

        // Prepared.pot files
        for (r in prepared_japanese) {    
            try {
                if (
                    // Check IDs
                    prepared_japanese[r].id.toUpperCase().includes(pattern.toUpperCase()) ||
        
                    // Check Data
                    prepared_chinese_traditional[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_chinese_traditional_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_english_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_english[r].msg.toUpperCase().includes(pattern.toUpperCase()) 
    
                ) {
                    matches_prepared.push(r);
                }
            } catch (err) {
                console.log("discord_matchbornes(): ERROR processing Prepared.pot matches: " + err);
            }
            
        }

        
        // Check for exact.
        if (exact == true) {
            exact_matches = [];
            exact_matches_prepared = [];
            try {
                // Remove all non-exact matches.
                console.log("- Removing non-exact matches. Pattern: " + JSON.stringify(pattern));
                for (var a in matches) {
                    //console.log("matches[a]: " + matches[a]);
                    //console.log("  - Check: " + japanese[matches[a]].id.trim().toUpperCase() + " -VS- " + pattern.toUpperCase());
                    if (japanese[matches[a]].id.trim().toUpperCase() == pattern.toUpperCase()) {
                        //console.log("  - FOUND!");
                        exact_matches.push(matches[a]);
                    }
                }
                for (var a in matches_prepared) {
                    //console.log("  - Check: " + JSON.stringify(prepared_japanese[matches_prepared[a]].id));
                    if (prepared_japanese[matches_prepared[a]].id.trim().toUpperCase() == pattern.toUpperCase()) {
                        //console.log("  - FOUND!");
                        exact_matches_prepared.push(matches_prepared[a]);
                    }
                }
                matches = exact_matches;
                matches_prepared = exact_matches_prepared;
            } catch (err) {
                console.log("discord_matchbornes(): ERROR filtering exact matches: " + err);
            }
        
        }        
        
        //console.log("matches:" + matches.length + "    " + JSON.stringify(matches));
    
        // Parse matches into string.
        //var response = html_home;

        var response = '**' + parseInt(matches.length + matches_prepared.length) + ' matchbornes, first result:**\n\n';
        if (matches.length == 0 && matches_prepared.length == 0) {
            response = "No results.";
            return response;
        } else if (matches.length + matches_prepared.length > 1) {
            uri = encodeURI('http://matchbornes.arfonzo.org/' + pattern);
            response += '_Full results:_ ' + uri + '\n\n';
        }
        
        
        // original.pot - first result.
        if (matches.length > 0) {
            
            response += '**__' + japanese[matches[0]].id + '__**\n'
                + '```\n' + parse_msg(japanese[matches[0]].msg) + '```\n'
                + '```\n' + parse_msg(english_current[matches[0]].msg) + '```\n'
                + '```\n' + parse_msg(chinese_traditional_current[matches[0]].msg) + '```\n'
                ;

            // mb url
            response += 'mb url: http://matchbornes.arfonzo.org/@' + japanese[matches[0]].id + '\n';

            // Check if image exists.
            var has_image = false;
            i = japanese[matches[0]].id.split('-')[1];
            if (i) {
                var imagefile = "./img/" + i.toUpperCase() + ".jpg";
                if (fs.existsSync(imagefile)) {
                    //console.log("TODO An image exists for this matchborne: " + imagefile);
                    has_image = true;
                }
            }
            if (has_image) {
                response += 'mb img: http://matchbornes.arfonzo.org/' + imagefile.substring(2) + '\n';
            }
        }

        // Prepared.pot records
        if (matches.length == 0 && matches_prepared.length > 0) {
            
            response += '**__' + prepared_japanese[matches_prepared[0]].id  + '__** `[Prepared.pot]`\n' 
                + '```\n' + parse_msg(prepared_japanese[matches_prepared[0]].msg) + '```\n'
                + '```\n' + parse_msg(prepared_english_current[matches_prepared[0]].msg) + '```\n'
                + '```\n' + parse_msg(prepared_chinese_traditional_current[matches_prepared[0]].msg) + '```\n'
                ;

            // mb url
            response += 'mb url: http://matchbornes.arfonzo.org/@' + prepared_japanese[matches_prepared[0]].id + '\n';

            // Check if image exists.
            var has_image = false;
            i = prepared_japanese[matches_prepared[0]].id.split('-')[1];
            if (i) {
                var imagefile = "./img/" + i.toUpperCase() + ".jpg";
                if (fs.existsSync(imagefile)) {
                    //console.log("TODO An image exists for this matchborne: " + imagefile);
                    has_image = true;
                }
            }
            if (has_image) {
                response += 'mb img: http://matchbornes.arfonzo.org/' + imagefile.substring(2) + '\n';
            }            
        }
    
        
        total_matches = parseInt(matches.length + matches_prepared.length);
        //console.log("Total matches: " + total_matches);
        if (total_matches > 1) {
            // Display alternates.
            //response = "";

            // Don't include the first match in more matches display.
            if (matches.length > 0) {
                matches = matches.slice(1);
            } else if (matches.length == 0 && matches_prepared.length > 0) {
                matches_prepared = matches_prepared.slice(1);
            }

            match_limit = 10;
            counter = 0;
            matches_combined = matches.concat(matches_prepared);

            response += "\n**More matchbornes:** ";
            
            for (a in matches) {
                //console.log("- checking " + a + " in matches...   counter:" + counter);
                if (counter < match_limit) {
                    if (a > 0) {
                        response += ', ';
                    }

                    if (english[matches[a]].msg.length < 32 && english[matches[a]].msg != "\"Preparing for translation…\"") {
                        response += ' _' + english[matches[a]].msg.replace(/"/g, '') + ':_ ';
                    }

                    response += '`' + japanese[matches[a]].id + '`';                    
                }
                counter+=1;
            }

            for (a in matches_prepared) {
                //console.log("-checking " + a + " in matches_prepared...");
                if (counter < match_limit) {
                    if (matches.length > 0 || a > 0) response += ', ';

                    if (prepared_english[matches_prepared[a]].msg.length < 32 && prepared_english[matches_prepared[a]].msg != "\"Preparing for translation…\"") {
                        response += ' _' + prepared_english[matches_prepared[a]].msg.replace(/"/g, '') + ':_ ';
                    }

                    response += '`' + prepared_japanese[matches_prepared[a]].id + '`';                    
                }
                counter+=1;
            }
            
            if (total_matches >= match_limit) {
                response += ' _... and more! See webpage for full results._';
            }
            response += '\n';
        }
        

        return response;
    } catch (err) {
        console.log("discord_matchbornes() ERROR: " + err);
        return "<p><strong>" + err.toString() + "</strong></p><p>I've been paralyzed! Try another search or summon @arfonzo#2710.</p>";
    }
    

}

function discord_matchbornes_category(category, pattern) {
    try {
        pattern = decodeURI(pattern);
        console.log("discord_matchbornes_category() called. Category: " + category + "\tPattern: " + pattern);

        var search_IDs = [];

        switch (category) {
            case searchtype.contract:
                search_IDs = contract_IDs;
                break;

            case searchtype.dungeon:
                search_IDs = dungeon_IDs;
                break;

            case searchtype.effect:
                search_IDs = effect_IDs;
                break;

            case searchtype.job:
                search_IDs = job_IDs;
                break;

            case searchtype.legendary:
                search_IDs = legendary_IDs;
                break;

            case searchtype.mod_enemy:
                search_IDs = enemymod_IDs;
                break;

            case searchtype.mod_skill:
                search_IDs = skillmod_IDs;
                break;

            case searchtype.skill:
                search_IDs = skill_IDs;
                break;
        }

        var response = "__**" + category + " matchbornes: (" + pattern + ")**__\n\n";
        var counter = 0;
        var max = 20;

        // Match filtered items.
        for (a in search_IDs) {
            // Only check "MasterNames"
            t = japanese[search_IDs[a]].id.split('-')[0];
            s = japanese[search_IDs[a]].id.split('-')[1];
            if (counter < max && t == "MasterName" && (
                japanese[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||

                english_current[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||
                english[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||

                chinese_traditional_current[search_IDs[a]].msg.includes(pattern)  ||
                chinese_traditional[search_IDs[a]].msg.includes(pattern)
                )) {
                response += '**' + english_current[search_IDs[a]].msg.substring(1, english_current[search_IDs[a]].msg.length - 1) + '** _' + s + '_';
                // Get matching desc
                for (b in search_IDs) { 
                    t2 = english_current[search_IDs[b]].id.split('-')[0];
                    if (t2 == "MasterDesc" && english_current[search_IDs[b]].id.includes(s)) {
                        //response += ': ' + english_current[search_IDs[b]].msg.substring(1, english_current[search_IDs[b]].msg.length - 1);
                        response += ': `' + parse_msg(english_current[search_IDs[b]].msg) + '`';
                    }
                }
                response += '\n';

                // Image for first response.
                if (counter == 0) {
                    var imagefile = "./img/" + s.toUpperCase() + ".jpg";
                    if (fs.existsSync(imagefile)) {
                        response += '_Image: http://matchbornes.arfonzo.org/' + imagefile.substring(2) + ' (shown below)_\n';
                    }
                }

                counter++;
            }
        }

        return response;
    } catch (err) {
        console.log("- ERROR: " + err);
    }
}

function parse_msg(msg) {
    //console.log("parse_msg(): " + JSON.stringify(msg));
    var msg2 = msg.replace(/\"\n\"/g, "").replace(/\\\"/g, "\"");
    //msg2 = msg2.substring(2, msg2.length-1);
    return msg2;
}
