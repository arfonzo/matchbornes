const fs = require('fs');
const http = require('http');
const url = require('url');
const ObjectsToCsv = require('objects-to-csv');

const banner = "    m a t c h b o r n e s";
const version = "0.7.2b";
console.log("");
console.log(banner);
console.log("        v" + version);
console.log("");

eval(fs.readFileSync('./config.js') + '');
//console.debug(JSON.stringify(mbConfig));
eval(fs.readFileSync('./discord.js') + '');

const hostname = '0.0.0.0';
const port = 3000;

const searchtype = {
    default: "Default",
    contract: "Contract",    
    dungeon: "Dungeon",
    effect: "Effect",    
    job: "Job",
    legendary: "Legendary Item",
    skill: "Skill",
    mod_enemy: "Enemy Modifier",
    mod_skill: "Skill Modifier",    
}

var lang;

var contract_IDs = [];
var dungeon_IDs = [];   // DG~
var effect_IDs = [];
var enemymod_IDs = [];  // EM~
var job_IDs = [];
var legendary_IDs = [];
var skill_IDs = [];
var skillmod_IDs = [];  // SM~

// TODO
var enemy_IDs;
var boss_IDs;

// Language-specific variables
var untranslated_en_msg = '"Preparing for translation…"';
var untranslated_en = [];
var untranslated_fr_msg = '"Préparation pour la traduction ..."';
var untranslated_fr = [];

function Record() {
    this.id = null;
    this.msg = null;
}

const styleblock = "<style>" +
    "body {" +
    "background-color: black;" +
    "color: white;" +
    "padding: 16px;" +
    "}" +
    "a, a:active, a:visited, a:hover { text-decoration: none; color: #55ff88}" +
    "</style>";

const html_home = "<p><a href=\"/\">matchbornes home</a></p>";

const html_form_search = '<form id="searchForm">' +
    '<input type="text" id="searchText">' +
    ' <select form="searchForm" id="searchType"><option value="default" selected>Everything</option><option value="contract">Contract</option><option value="dungeon">Dungeon</option><option value="effect">Effect</option><option value="mod_enemy">Enemy Modifier</option><option value="job">Job</option><option value="legendary">Legendary</option><option value="skill">Skill</option><option value="mod_skill">Skill Modifier</option></select>' +
    ' <input type="submit" id="submitSearch" value="search">' +
    '<br/><span style="font-size:small">[ <a href="https://bitbucket.org/arfonzo/matchbornes/wiki/Home" target="_blank">help</a> | <a href="https://bitbucket.org/arfonzo/matchbornes/wiki/ChangeLog" target="_blank">changelog</a> ]</span>' +            
    '</form>';

const html_lore = get_lore();

const easter_eggs = ["DEBUG", "SHADOW"];
const specials = ["CSV"];
var csv_data = null;

function debug_records_loaded() {
    console.log("- Records found per language, original.pot: " + japanese.length);
    console.log("- Records found per language, Prepared.pot: " + prepared_japanese.length);
    console.log("- [TODO] Boss Records found: " + boss_IDs.length);
    console.log("- Contract Records found: " + contract_IDs.length);
    console.log("- Dungeon Records found: " + dungeon_IDs.length);
    console.log("- Effects Records found: " + effect_IDs.length);
    console.log("- [TODO] Enemy Records found: " + enemy_IDs.length);
    console.log("- Enemy Modifier Records found: " + enemymod_IDs.length);
    console.log("- Job Records found: " + job_IDs.length);
    console.log("- Legendary Effect Records found: " + legendary_IDs.length);
    console.log("- Skill Records found: " + skill_IDs.length);
    console.log("- Skill Modifier Records found: " + skillmod_IDs.length);
}

function get_debug() {
    response = '<h1>Debug Stuff</h1>';

    response += '<pre>';
    response += '<h2>Core</h2>';
    //response += '<li></li>';
    response += '<li>version: ' + version + '</li>';
    response += '<li>uptime: ' + Math.floor(process.uptime()) + 's</li>';
    response += '<li>memHeap: ' + parseInt(process.memoryUsage().heapUsed / 1024 / 1024) + 'MB</li>';
    response += '<li>cpuUsage:<br/>' + JSON.stringify(process.cpuUsage(), null, 2) + '</li>';
    
    if (bot) {
        response += '<h2>Discord</h2>';
        response += '<li>verified: ' + bot.verified  + '</li>';
        response += '<li>connected: ' + bot.connected  + '</li>';
        //response += '<li>presence: ' + bot.presenceStatus  + '</li>';
    }

    response += '<h2>Stateful Potatoes</h2>';
    response += '<li>Records per language, original.pot: ' + japanese.length + '</li>';
    response += '<li>Records per language, Prepared.pot: ' + prepared_japanese.length + '</li>';
    response += '<li>Boss Records found: ' + boss_IDs.length  + '</li>';
    response += '<li>Contract Records found: ' + contract_IDs.length  + '</li>';
    response += '<li>Dungeon Records found: ' + dungeon_IDs.length + '</li>';
    response += '<li>Effects Records found: ' + effect_IDs.length + '</li>';
    response += '<li>Enemy Modifier Records found: ' + enemymod_IDs.length + '</li>';
    response += '<li>Enemy Records found: ' + enemy_IDs.length + '</li>';
    response += '<li>Job Records found: ' + job_IDs.length + '</li>';
    response += '<li>Legendary Effect Records found: ' + legendary_IDs.length + '</li>';
    response += '<li>Skill Records found: ' + skill_IDs.length + '</li>';
    response += '<li>Skill Modifier Records found: ' + skillmod_IDs.length + '</li>';
    response += '<li>untranslated_en: ' + untranslated_en.length + ' (' + ((untranslated_en.length/english_current.length)* 100).toFixed(2) + '%)</li>';
    response += '<li>untranslated_fr: ' + untranslated_fr.length + ' (' + ((untranslated_fr.length/french_current.length)* 100).toFixed(2) + '%)</li>';
    response += '</pre>';
    return response;
}

function get_egg(egg) {
    console.log("get_egg() called: " + egg);
    var response = html_home;
    switch (egg) {
        case "DEBUG":
            response += get_debug();
            break;

        case "SHADOW":
            response +=  "<h1>Shadow the Cat</h1><p>A random picture of nekobornes.</p>";
            // Image
            // Get all images in directory.
            var images = fs.readdirSync("./img_shadow/", "utf8");
            images.splice(images.indexOf("Thumbs.db"), 1);
            // Display a random one
            for (var i = 0; i < 1; i++) {
                response += '<img src="/img_shadow/' + images[Math.floor(Math.random() * images.length)] + '" style="max-height:75%; max-width:75%; padding:32px;"/>';
            }
            break;

        default:
            break;

    }

    return response;
}

function special_action(action) {
    console.log("special_action() called: " + action);

    switch (action) {
        case "CSV":
            response = special_csv().toString();
            break;

        default:
            response = null;
            break;

    }    
    return response;
}

function special_csv() {

    // Sample data - two columns, three rows:
    /*const data = [
        {code: 'HK', name: 'Hong Kong'},
        {code: 'KLN', name: 'Kowloon'},
        {code: 'NT', name: 'New Territories'},
    ];*/

    switch(lang.toUpperCase()) {
        case "EN":
            data = english_current;
            break;
        
        case "JA":
            data = japanese;
            break;

        default:
            data = [];
            break;
    }

    // TODO: only works first time on instance. Then it's old csv_data.
    if (csv_data == null) csv_data = "<h1>Reload page for results.</h1>";

    // Function must be asynchronous:
    (async() =>{
        let csv = new ObjectsToCsv(data);
    
        // Save to file:
        //await csv.toDisk('./test.csv');
    
        // Return the CSV file as string:
        //console.log(await csv.toString());

        var r = '<pre>';
        r += await csv.toString();
        r += '</pre>';

        csv_data = r;
    })();

    //TODO: await for call before returning, otherwise a page refresh is needed.
    return csv_data;
}

function get_lore() {
    /*
    dark elf  "TEXT_BODY_EVENT_JOB_DARKELF"
    TEXT_BODY_EVENT_JOB_DARKELF_0 to 10.

    Dragoon "TEXT_BODY_EVENT_JOB_DRAGOON"

    Saint "TEXT_BODY_EVENT_JOB_SAINT"
    */
   var result = "<h1>Lore</h1>"
    +'<p>You may find the following links of interest:</p>'
    //+'<li>TODO</li>'
    +'<h3>Jobs</h3>'
    +'<li>Dark Elf: <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_0">0</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_1">1</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_2">2</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_3">3</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_4">4</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_5">5</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_6">6</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_7">7</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_8">8</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_9">9</a> <a href="/@TEXT_BODY_EVENT_JOB_DARKELF_10">10</a></li>'
    +'<li>Dragoon: <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_0">0</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_1">1</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_2">2</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_3">3</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_4">4</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_5">5</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_6">6</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_7">7</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_8">8</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_9">9</a> <a href="/@TEXT_BODY_EVENT_JOB_DRAGOON_10">10</a></li>'
    +'<li>Saint: <a href="/@TEXT_BODY_EVENT_JOB_SAINT_0">0</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_1">1</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_2">2</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_3">3</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_4">4</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_5">5</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_6">6</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_7">7</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_8">8</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_9">9</a> <a href="/@TEXT_BODY_EVENT_JOB_SAINT_10">10</a></li>'
    ;
    

    /*
    rabbit ask for frags"ES000169-ES000178"    giving gift back"ES000180"    refuse"ES000185"
    ↑Regular dungeon events, can be trigger in any dungeon.
    */
    result += '<h3>Regular Dungeon Events</h3><p>These events can be triggered from any Dungeon.</p>';
    events_rabbit = [];
    for (var a = 169; a <= 178; a++) {
        events_rabbit.push("MasterDesc-ES000" + a.toString());
    }
    events_rabbit.push("MasterDesc-ES000180");
    events_rabbit.push("MasterDesc-ES000185");
    result += '<li>Rabbit:';
    for (a in events_rabbit) {
        result += ' <a href="/@' + events_rabbit[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';
    //console.log("events_rabbit: " + JSON.stringify(events_rabbit));

    result += '<h3>Season Events</h3>'
    /*
    season glacier and Eastern dark priest and the monster bay "ESSE0801-ESSE0808"    second choice "look down" not kill "ESSE0809-ESSE0810"
    */
    result += '<p>Glacier and Eastern Seasons - Dark Priest and Cursed Child:</p>';
    events_a = []
    for (var a = 1; a <= 10; a++) {
        events_a.push("MasterDesc-ESSE080" + a.toString());
    }
    result += '<li>';
    for (a in events_a) {
        result += ' <a href="/@' + events_a[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';

    /*

    season glacier and Eastern angel   "ESSE1001-ESSE1012"
    */
    result += '<p>Glacier and Eastern - Angel:</p>';
    events_ge = []
    for (var a = 1; a <= 12; a++) {
        events_ge.push("MasterDesc-ESSE10" + ("0" + a).slice(-2));
    }
    result += '<li>';
    for (a in events_ge) {
        result += ' <a href="/@' + events_ge[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';

    /*

    season Eastern  Raiment contract "ESSE1101-ESSE1112" */
    result += '<p>Eastern - Raiment Contract</p>';
    events_er = []
    for (var a = 1; a <= 12; a++) {
        events_er.push("MasterDesc-ESSE11" + ("0" + a).slice(-2));
    }
    result += '<li>';
    for (a in events_er) {
        result += ' <a href="/@' + events_er[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';


    /*    season Spire  Elf mega "ESSE1201-ESSE1206" */
    result += '<p>Spire - Elf Mega</p>';
    events_sem = []
    for (var a = 1; a <= 6; a++) {
        events_sem.push("MasterDesc-ESSE12" + ("0" + a).slice(-2));
    }
    result += '<li>';
    for (a in events_sem) {
        result += ' <a href="/@' + events_sem[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';

    /*  season Spire  automata "ESSE1207-ESSE1212"   maybe put  the dirty doll's describe "ENE00124" would be better, it's a guess , that maybe automata made by Elf mega.*/
    result += '<p>Spire - Automata</p>';
    events_sa = []
    for (var a = 7; a <= 12; a++) {
        events_sa.push("MasterDesc-ESSE12" + ("0" + a).slice(-2));
    }
    result += '<li>';
    for (a in events_sa) {
        result += ' <a href="/@' + events_sa[a] + '">' + a.toString() + '</a>';
    }
    result += '</li>';

    /*Valentine Day's event
    druid ESSS0101-ESSS0103
    samurai ESSS0104-ESSS0107
    witch?  ESSS0107-ESSS0109
    Saint ESSS0110-ESSS0112
    Fairy ESSS0113-ESSS0115
    valkiry    ESSS0116-ESSS0118
    vampire ESSS0119-ESSS0121
    Uncle Valentine / saint Valentine?   ESSS0122-ESSS0125*/
    result += '<p>Valentine\'s Day event:</p>';
    events_vd = {
        "Druid": [1, 3],
        "Samurai": [4,6],
        "Witch": [7,9],
        "Saint": [10, 12],
        "Fairy": [13, 15],
        "Valkyrie": [16,18],
        "Vampire": [19, 21],
        "Uncle Valentine": [22, 25]
    }
    for (a in events_vd) {
        result += '<li>';
        result += a + ':';
        counter = 0;
        low = events_vd[a][0];
        high = events_vd[a][1];
        for (var i = low; i <= high; i++) {            
            id = "MasterDesc-ESSS01" + ("0" + i).slice(-2);
            result += ' <a href="/' + id + '">' + counter + '</a>';
            counter++;
        }
        result += '</li>';
    }

    /*white Valentine Day's event
    warrior  ESSS0201-ESSS0203
    Ranger  ESSS0204-ESSS0206
    mage  ESSS0207-ESSS0209
    Ninja ESSS0210-ESSS0212
    Necromancer  ESSS0213-ESSS0215
    Evil knight ESSS0216-ESSS0218
    Rover ESSS0219 -ESSS0221
    Grave robberESSS0222-ESSS0224
    */
   result += '<p>White Day event:</p>';
   events_vdw = {
       "Warrior": [1, 3],
       "Ranger": [4,6],
       "Mage": [7,9],
       "Ninja": [10, 12],
       "Necromancer": [13, 15],
       "Evil Knight": [16,18],
       "Rover": [19, 21],
       "Grave Robber": [22, 24]
   }
   for (a in events_vdw) {
       result += '<li>';
       result += a + ':';
       counter = 0;
       low = events_vdw[a][0];
       high = events_vdw[a][1];
       for (var i = low; i <= high; i++) {            
           id = "MasterDesc-ESSS02" + ("0" + i).slice(-2);
           result += ' <a href="/' + id + '">' + counter + '</a>';
           counter++;
       }
       result += '</li>';
   }

    result += '<br/><br/><br/><br/><br/><span style="font-size:small"><a href="/@SHADOW">Shadow</a></span>';

    return result;
}

function is_effect(game_id) {
    i = game_id.split('-')[1];
    if (i && i.substring(0.1) == "EF") {
        return true;
    }      
    return false;
}

function is_skill(game_id) {
    i = game_id.split('-')[1];
    if (i && i.substring(0.1) == "SK") {
        return true;
    }      
    return false;
}


/*function match_data(datafile, index, pattern) {
    return datafile[index].msg.includes(pattern);
}*/

function matchbornes(pattern, exact) {
    try {
        //pattern = pattern.replace(/%20/g, " ");
        pattern = decodeURI(pattern);
        console.log("matchbornes() called: " + JSON.stringify(pattern) + "\t Exact: " + exact);
        //console.log("- Decoded: " + decodeURI(pattern));
    
        if (exact && easter_eggs.indexOf(pattern.toUpperCase())!= -1) {
            return get_egg(pattern.toUpperCase());
        } else if (exact && specials.indexOf(pattern.toUpperCase())!= -1) {
            return special_action(pattern.toUpperCase());
        }
    
        var matches = [];
        var matches_prepared = [];
    
        for (r in japanese) {
            try {
		    if (
                    // Check IDs
                    japanese[r].id.toUpperCase().includes(pattern.toUpperCase()) ||
        
                    // Check Data
                    japanese[r].msg.includes(pattern) ||
                    //match_data(japanese, r, pattern) ||
                    chinese_traditional[r].msg.includes(pattern) ||
                    chinese_traditional_current[r].msg.includes(pattern) ||
                    //|| chinese_simplified[r].msg.includes(pattern)                
                    korean[r].msg.includes(pattern) ||
                    english_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    english[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    french_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    french[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    spanish[r].msg.toUpperCase().includes(pattern.toUpperCase())
    
		    ) {
                    matches.push(r);
                }
            } catch (err) {
                console.log("matchbornes(): ERROR processing original.pot matches: " + err + "\t" + JSON.stringify(japanese[r]));
            }
    

        }

        // Prepared.pot files
        for (r in prepared_japanese) {    
            try {
                if (
                    // Check IDs
                    prepared_japanese[r].id.toUpperCase().includes(pattern.toUpperCase()) ||
        
                    // Check Data
                    prepared_chinese_traditional[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_chinese_traditional_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_english_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_english[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_french_current[r].msg.toUpperCase().includes(pattern.toUpperCase()) ||
                    prepared_french[r].msg.toUpperCase().includes(pattern.toUpperCase()) 
    
                ) {
                    matches_prepared.push(r);
                }
            } catch (err) {
                console.log("matchbornes(): ERROR processing Prepared.pot matches: " + err);
            }
            
        }

        
        // Check for exact.
        if (exact == true) {
            exact_matches = [];
            exact_matches_prepared = [];
            try {
                // Remove all non-exact matches.
                //console.log("- Removing non-exact matches. Pattern: " + JSON.stringify(pattern));
                for (var a in matches) {
                    //console.log("matches[a]: " + matches[a]);
                    //console.log("  - Check: " + japanese[matches[a]].id.trim().toUpperCase() + " -VS- " + pattern.toUpperCase());
                    if (japanese[matches[a]].id.trim().toUpperCase() == pattern.toUpperCase()) {
                        //console.log("  - FOUND!");
                        exact_matches.push(matches[a]);
                    }
                }
                for (var a in matches_prepared) {
                    //console.log("  - Check: " + JSON.stringify(prepared_japanese[matches_prepared[a]].id));
                    if (prepared_japanese[matches_prepared[a]].id.trim().toUpperCase() == pattern.toUpperCase()) {
                        //console.log("  - FOUND!");
                        exact_matches_prepared.push(matches_prepared[a]);
                    }
                }
                matches = exact_matches;
                matches_prepared = exact_matches_prepared;
            } catch (err) {
                console.log("matchbornes(): ERROR filtering exact matches: " + err);
            }
        
        }        
        
    
        //console.log("matches:" + matches.length + "    " + JSON.stringify(matches));
    
        // Parse matches into string.
        var response = html_home;
    
        max = 100;
        if (matches.length > max) {
            // Only return the first 100 hits.
            len = matches.length;
            matches.splice(max);
            response += len + " results found in original.pot: limiting to " + max + ".";
        }
        if (matches_prepared.length > max) {
            // Only return the first 100 hits.
            len = matches_prepared.length;
            matches_prepared.splice(max);
            response += len + " results found in Prepared.pot: limiting to " + max + ".";
        }
    
        for (var m in matches) {
            try {
                // Check if image exists.
                var has_image = false;
                i = japanese[matches[m]].id.split('-')[1];
                if (i) {
                    var imagefile = "./img/" + i.toUpperCase() + ".jpg";
                    if (fs.existsSync(imagefile)) {
                        //console.log("TODO An image exists for this matchborne: " + imagefile);
                        has_image = true;
                    }
                }
        
        
                response += "<h1>" + japanese[matches[m]].id + "</h1>";
        
                if (has_image) {
                    //response += '<img height=256px src="' + imagefile + '"/>';
                    response += '<img src="' + imagefile + '" style="max-height:25%; max-width:25%"/>';
                }
        
                response += "<h3>ja</h3>";
                response += '<pre>' + japanese[matches[m]].msg + "</pre>";
        
                response += "<h3>en</h3>";
                response += "<pre>" + english_current[matches[m]].msg + "</pre>";        
                // Show STABLE branch string if different.
                if (english_current[matches[m]].msg != english[matches[m]].msg) {
                    response += "<span style=\"color: #888; padding-left:32px;\"><strong>Current in-game string:</strong> ";
                    response += "<pre style=\"padding-left:32px;\">" + english[matches[m]].msg + "</pre></span>";
                }
        
                //response += "<h3>zh (simplified)</h3>";
                //response += "<pre>" + chinese_simplified[matches[m]].msg + "</pre>";
        
                response += "<h3>zh-t</h3>";
                response += "<pre>" + chinese_traditional_current[matches[m]].msg + "</pre>";
                // Show STABLE branch string if different.
                if (chinese_traditional_current[matches[m]].msg != chinese_traditional[matches[m]].msg) {
                    response += "<span style=\"color: #888; padding-left:32px;\"><strong>Current in-game string:</strong> ";
                    response += "<pre style=\"padding-left:32px;\">" + chinese_traditional[matches[m]].msg + "</pre></span>";
                }

                response += "<h3>fr</h3>";
                response += "<pre>" + french_current[matches[m]].msg + "</pre>";
                // Show STABLE branch string if different.
                if (french_current[matches[m]].msg != french[matches[m]].msg) {
                    response += "<span style=\"color: #888; padding-left:32px;\"><strong>Current in-game string:</strong> ";
                    response += "<pre style=\"padding-left:32px;\">" + french[matches[m]].msg + "</pre></span>";
                }
        
                response += "<h3>es</h3>";
                response += "<pre>" + spanish[matches[m]].msg + "</pre>";

                response += "<h3>ko</h3>";
                response += "<pre>" + korean[matches[m]].msg + "</pre>";
        
                response += "<hr/>";
            } catch (err) {
                console.log("matchbornes(): ERROR processing matches: " + err);
            }
        }

        // Prepared.pot records
        for (var m in matches_prepared) {
            try {
                // Check if image exists.
                var has_image = false;
                i = prepared_japanese[matches_prepared[m]].id.split('-')[1];
                if (i) {
                    var imagefile = "./img/" + i.toUpperCase() + ".jpg";
                    if (fs.existsSync(imagefile)) {
                        //console.log("TODO An image exists for this matchborne: " + imagefile);
                        has_image = true;
                    }
                }    
        
                response += "<h1>" + prepared_japanese[matches_prepared[m]].id + "</h1>";
                response += "<pre>[Prepared.pot]</pre><br/>";
        
                if (has_image) {
                    response += '<img src="' + imagefile + '" style="max-height:25%; max-width:25%"/>';
                }
        
                response += "<h3>ja</h3>";
                response += '<pre>' + prepared_japanese[matches_prepared[m]].msg + "</pre>";
        
                response += "<h3>en</h3>";
                response += "<pre>" + prepared_english_current[matches_prepared[m]].msg + "</pre>";
        
                // Show STABLE branch string if different.
                if (prepared_english_current[matches_prepared[m]].msg != prepared_english[matches_prepared[m]].msg) {
                    response += "<h3>en (IN-GAME)</h3>";
                    response += "<pre>" + prepared_english[matches_prepared[m]].msg + "</pre>";
                }

                response += "<h3>zh-t</h3>";
                response += "<pre>" + prepared_chinese_traditional_current[matches_prepared[m]].msg + "</pre>";
                
                // Show STABLE branch string if different.
                if (prepared_chinese_traditional_current[matches_prepared[m]].msg != prepared_chinese_traditional[matches_prepared[m]].msg) {
                    response += "<h3>zh-t (IN-GAME)</h3>";
                    response += "<pre>" + prepared_chinese_traditional[matches_prepared[m]].msg + "</pre>";
                }
                
        
                response += "<hr/>";
            } catch (err) {
                console.log("matchbornes(): ERROR processing matches_prepared: " + err);
            }
            
        }
    
        return response;
    } catch (err) {
        console.log("matchbornes() ERROR: " + err);
        return "<p><strong>" + err.toString() + "</strong></p><p>I've been paralyzed! Try another search or summon @arfonzo#2710.</p>";
    }
}

function matchbornes_category(category, pattern) {
    try {
        pattern = decodeURI(pattern);
        console.log("matchbornes_category() called. Category: " + category + "\tPattern: " + pattern);

        var search_IDs = [];

        switch (category) {
            case searchtype.contract:
                search_IDs = contract_IDs;
                break;

            case searchtype.dungeon:
                search_IDs = dungeon_IDs;
                break;

            case searchtype.effect:
                search_IDs = effect_IDs;
                break;

            case searchtype.job:
                search_IDs = job_IDs;
                break;

            case searchtype.legendary:
                search_IDs = legendary_IDs;
                break;

            case searchtype.skill:
                search_IDs = skill_IDs;
                break;

            case searchtype.mod_enemy:
                search_IDs = enemymod_IDs;
                break;

            case searchtype.mod_skill:
                search_IDs = skillmod_IDs;
                break;
                
        }

        var response = "<h1>" + category + " matchbornes: " + pattern + "</h1>";
        
        var counter = 0;
        var max = 20;

        // Match skill.
        for (a in search_IDs) {
            // Only check "MasterNames"
            t = japanese[search_IDs[a]].id.split('-')[0];
            s = japanese[search_IDs[a]].id.split('-')[1];
            if (counter < max && t == "MasterName" && (
                japanese[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||

                english_current[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||
                english[search_IDs[a]].msg.toUpperCase().includes(pattern.toUpperCase())  ||

                chinese_traditional_current[search_IDs[a]].msg.includes(pattern)  ||
                chinese_traditional[search_IDs[a]].msg.includes(pattern)
                )) {
                // Name
                response += '<h2>' + english_current[search_IDs[a]].msg.substring(1, english_current[search_IDs[a]].msg.length - 1) + '</h2>';

                // Check if image exists.
                var imagefile = "./img/" + s.toUpperCase() + ".jpg";
                if (fs.existsSync(imagefile)) {
                        response += '<img src="' + imagefile + '" style="max-height:25%; max-width:25%"/>';
                }

                // ID
                response += '<p><a href=/' + s + '>' + s + '</a></p>';

                // Get matching desc
                for (b in search_IDs) { 
                    t2 = english_current[search_IDs[b]].id.split('-')[0];
                    if (t2 == "MasterDesc" && english_current[search_IDs[b]].id.includes(s)) {
                        //response += '<p>' + english_current[search_IDs[b]].msg.substring(1, english_current[search_IDs[b]].msg.length - 1) + '</p>';
                        response += '<p>' + parse_msg(english_current[search_IDs[b]].msg) + '</p>';
                    }
                }
                //response += '\n';

                response += '<hr/>';
                counter++;
            }
        }

        return response;
    } catch (err) {
        console.log("- ERROR: " + err);
    }
}


function readPotFile(filename) {
    try {
        records = [];
        data = fs.readFileSync(filename, "utf8");
        //console.log("data:" + data.length);

        arr = data.split("\n\n");
        //console.log("arr length: " + arr.length);

        for (a in arr) {
            if (arr[a]) {
                try {
                    var record = new Record();

                    //if (a == 1) {
                    // Parse record from strings.
                    //console.log(JSON.stringify(arr[a]));

                    //var id = arr[a].split('msgid \"')[1].replace('"', '');
                    var id = arr[a].split('msgid \"')[1].split('\n')[0].replace("\"", '');
                    var str = arr[a].split('\nmsgstr')[1].trim().replace("\"\"", '');
                    //console.log("msgid:" + id);
                    //console.log("msgstr:" + JSON.stringify(str));

                    record.id = id;
                    record.msg = str;
                    records.push(record);
                    //}
                } catch (e) {
                    console.log("Error reading record index " + a + ": " + e);
                }
            
            }

        }

        // Remove first record
        records.splice(0, 1);

        //console.log("Records added: " + records.length);
        //console.log("- Skills added: " + skill_IDs.length);

        return records;
    } catch (err) {
        console.log("readPotFile() ERROR: " + err);
        return [];
    }
}

function readPotFiles() {
    try {
        console.log(Date.now() + " readPotFiles() called.");

        /* original.pot files */
        japanese = readPotFile("translations/JA/original.pot");
        english = readPotFile("translations/EN/original.pot"); // Previous EN version (vanilla in-game: no changes from current)
        english_current = readPotFile("translations/EN/original_current.pot");
        //chinese_simplified = readPotFile("translations/CHS/original.pot");
        chinese_traditional = readPotFile("translations/CHT/original.pot");
        chinese_traditional_current = readPotFile("translations/CHT/original_current.pot");
        french = readPotFile("translations/FR/original.pot");
        french_current = readPotFile("translations/FR/original_current.pot");
        korean = readPotFile("translations/KO/original.pot");
        spanish = readPotFile("translations/ES/original.pot");

        /* Prepared.pot files */
        prepared_japanese = readPotFile("translations/JA/Prepared.pot");
        prepared_english = readPotFile("translations/EN/Prepared.pot");
        prepared_english_current = readPotFile("translations/EN/Prepared_current.pot");
        prepared_chinese_traditional = readPotFile("translations/CHT/Prepared.pot");
        prepared_chinese_traditional_current = readPotFile("translations/CHT/Prepared_current.pot");
        prepared_french = readPotFile("translations/FR/Prepared.pot");
        prepared_french_current = readPotFile("translations/FR/Prepared_current.pot");

        // Filter for search types

        contract_IDs = [];
        dungeon_IDs = [];
        effect_IDs = [];
        enemymod_IDs = [];
        job_IDs = [];
        legendary_IDs = [];
        skill_IDs = [];
        skillmod_IDs = [];        
        // TODO
        enemy_IDs = [];
        boss_IDs = [];

        for (a in japanese) {
            i = japanese[a].id.split('-')[1];
            if (i) {
                if (i.length == 8 && i.substring(0,2) == "DG") {
                    // Dungeons
                    dungeon_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "EF") {
                    // Effects
                    effect_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "EM") {
                    // Enemy Modifiers
                    enemymod_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "EN") {
                    // Enemies
                    enemy_IDs.push(a);                    
                } else if (i.length == 8 && i.substring(0,2) == "FD") {
                    // Contracts
                    contract_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "JB") {
                    // Jobs
                    job_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "SK") {
                    // Skills
                    skill_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,2) == "SM") {
                    // Skill modifiers
                    skillmod_IDs.push(a);                    
                }

                if (i.length == 8 && i.substring(0,3) == "EFL") {
                    // Legendary
                    legendary_IDs.push(a);
                } else if (i.length == 8 && i.substring(0,3) == "ENB") {
                    // Bosses
                    boss_IDs.push(a);                    
                }
            }
        }

        /*
        console.log("- Contract Records found: " + contract_IDs.length);
        console.log("- Dungeon Records found: " + dungeon_IDs.length);
        console.log("- Effects Records found: " + effect_IDs.length);
        console.log("- Enemy Modifier Records found: " + enemymod_IDs.length);
        console.log("- Job Records found: " + job_IDs.length);
        console.log("- Legendary Effect Records found: " + legendary_IDs.length);
        console.log("- Skill Records found: " + skill_IDs.length);
        console.log("- Skill Modifier Records found: " + skillmod_IDs.length);
        */
        
        //console.log(JSON.stringify(skill_IDs));

        untranslated_en = [];
        untranslated_fr = [];

        for (a in english_current) {
            if (english_current[a].msg == untranslated_en_msg) {
                untranslated_en.push(a);
            }
        }

        for (a in french_current) {
            if (french_current[a].msg == untranslated_fr_msg) {
                untranslated_fr.push(a);
            }
        }
    } catch (err) {
        console.log("readPotFiles() ERROR: " + err);
        return [];
    }
}


/* Start web server */
const server = http.createServer((req, res) => {
    console.log(req.url);
    var response = "";
    var sent = false;

    var searchtype_contract = false;
    var searchtype_dungeon = false;
    var searchtype_effect = false;
    var searchtype_enemymod = false;
    var searchtype_job = false;
    var searchtype_legendary = false;
    var searchtype_skill = false;
    var searchtype_skillmod = false;

    /*var ip = req.headers['x-forwarded-for'].split(',').pop() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress
        ;    
    console.log("ip: " + ip);*/

    //console.log("- parsed URL: " + JSON.stringify(url.parse(req.url, true)));
    var urlPath = url.parse(req.url, true).pathname;
    var queryData = url.parse(req.url, true).query;
    if (Object.keys(queryData).length > 0) console.log("- queryData: " + JSON.stringify(queryData));
    
    if (queryData["contract"] != undefined || queryData["c"] != undefined) {
        console.log("- Contract search requested.");
        searchtype_contract = true;
    } else if (queryData["dungeon"] != undefined || queryData["d"] != undefined) {
        console.log("- Dungeon search requested.");
        searchtype_dungeon = true;   
    } else if (queryData["effect"] != undefined || queryData["e"] != undefined) {
        console.log("- Effect search requested.");
        searchtype_effect = true;   
    } else if (queryData["job"] != undefined || queryData["j"] != undefined) {
        console.log("- Job search requested.");
        searchtype_job = true;
    } else if (queryData["legendary"] != undefined || queryData["l"] != undefined) {
        console.log("- Legendary search requested.");
        searchtype_legendary = true;
    } else if (queryData["mod_enemy"] != undefined || queryData["em"] != undefined) {
        console.log("- Enemy Modifier search requested.");
        searchtype_enemymod = true;
    } else if (queryData["mod_skill"] != undefined || queryData["sm"] != undefined) {
        console.log("- Skill Modifier search requested.");
        searchtype_skillmod = true;
    } else if (queryData["skill"] != undefined || queryData["s"] != undefined) {
        console.log("- Skill search requested.");
        searchtype_skill = true;
    } else if (queryData["lang"] != undefined) {
        console.log("- Language requested: " + queryData["lang"].toUpperCase() );
        lang = queryData["lang"].toUpperCase();
    }


    // Check for hosted content to serve
    dir = req.url.split('/')[1];
    if (urlPath === "/favicon.ico") {
        // favicon.ico
    } else if (urlPath == "/") {
        //
        // Root: search and help page.
        //

        response = "";

        //response += "debug: " + JSON.stringify(images);

        response += "<h1>matchbornes</h1>";

        // Search box
        response += html_form_search;            

        // Image
        // Get all images in directory.
        images = fs.readdirSync("./img/", "utf8");
        images.splice(images.indexOf("Thumbs.db"), 1);
        // Display a random one
        for (var i = 0; i < 1; i++) {
            response += '<img src="/img/' + images[Math.floor(Math.random() * images.length)] + '" style="max-height:15%; max-width:15%; padding:32px;"/>';
        }        

        // Help
        //response += help_html;

        // Lore
        response += html_lore;

        // Javascript
        response += '<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>'
        response += '<script type="text/javascript">' +
            "$('#submitSearch').click(function(event) {" +
            "event.preventDefault();" +
            "if (document.getElementById(\"searchText\").value.length > 0) {" +
            'var qr = "";' +

            'if (document.getElementById("searchType").value == "contract") { qr = "?contract"; } ' +
            'if (document.getElementById("searchType").value == "dungeon") { qr = "?dungeon"; } ' +
            'if (document.getElementById("searchType").value == "effect") { qr = "?effect"; } ' +
            'if (document.getElementById("searchType").value == "job") { qr = "?job"; } ' +
            'if (document.getElementById("searchType").value == "legendary") { qr = "?legendary"; } ' +
            'if (document.getElementById("searchType").value == "skill") { qr = "?skill"; } ' +
            'if (document.getElementById("searchType").value == "mod_enemy") { qr = "?mod_enemy"; } ' +
            'if (document.getElementById("searchType").value == "mod_skill") { qr = "?mod_skill"; } ' +

            'window.location.href = window.location.href + document.getElementById("searchText").value + qr;' +
            "}" +
            "})" +
            "</script>";
    } else if (dir === "img" || dir === "img_shadow") {
        // /img/ path
        imagefile = urlPath.split('/').splice(0, 3).join('/');

        //console.log("TODO: hosting /img/ called: " + req.url + "    file: " + imagefile);

        try {
            var img = fs.readFileSync('.' + imagefile);
            res.writeHead(200, {
                'Content-Type': 'image/gif'
            });
            res.end(img, 'binary');
            sent = true;
        } catch (e) {
            console.log("/img/ error: " + e);
        }


    } else if (urlPath.length > 1) {
        if (searchtype_skill) {
            response = html_home;
            //response += '<pre>' + matchbornes_skill_name(urlPath.substring(1)) + '</pre>';
            response += '<pre>' + matchbornes_category(searchtype.skill, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_dungeon) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.dungeon, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_effect) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.effect, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_legendary) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.legendary, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_contract) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.contract, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_job) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.job, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_enemymod) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.mod_enemy, urlPath.substring(1)) + '</pre>';
        } else if (searchtype_skillmod) {
            response = html_home;
            response += '<pre>' + matchbornes_category(searchtype.mod_skill, urlPath.substring(1)) + '</pre>';
        } else {
            // Regular matchbornes search.
            exact = false;
            index = 1;
            if (urlPath[1] == '@') {
                // Exact match
                exact = true;
                index = 2;
            }
            response = matchbornes(urlPath.substring(index), exact);        
        }
        
    }

    if (!sent) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.setHeader('X-Powered-By', 'matchbornes');
        res.end('<html><head><meta charset="utf-8"><title>matchbornes</title>' + styleblock + '</head><body>' + response + '</body></html>');
    }

});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});


readPotFiles();
debug_records_loaded();

// Timers
timer = setInterval(function () {
    // EVERY 5 MINUTES
    readPotFiles();
}, 60000*5);
